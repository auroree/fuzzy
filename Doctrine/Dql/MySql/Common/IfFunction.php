<?php

namespace Mazurkiewicz\FuzzyBundle\Doctrine\Dql\MySql\Common;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Provides IF function.
 * Produces native MySQL expression.
 */
class IfFunction extends FunctionNode
{
    /**
     * Condition expression.
     *
     * @var string
     */
    private $condition;

    /**
     * Expression to evaluate if true.
     *
     * @var string
     */
    private $trueResult;

    /**
     * Expression to evaluate if false.
     *
     * @var string
     */
    private $falseResult;

    /**
     * Parses DQL expression.
     *
     * @param Parser $parser DQL parser
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->condition = $parser->ConditionalExpression();
        $parser->match(Lexer::T_COMMA);
        $this->trueResult = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->falseResult = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * Produces SQL expression.
     *
     * @param SqlWalker $sqlWalker Sql helper class
     *
     * @return string MySQL native expression
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return 'IF('.$this->condition->dispatch($sqlWalker).', '
            .$this->trueResult->dispatch($sqlWalker).', '
            .$this->falseResult->dispatch($sqlWalker).')';
    }
}
