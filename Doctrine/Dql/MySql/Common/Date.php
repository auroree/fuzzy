<?php

namespace Mazurkiewicz\FuzzyBundle\Doctrine\Dql\MySql\Common;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Provides DATE function.
 * Produces native MySQL expression.
 */
class Date extends FunctionNode
{
    /**
     * Function parameter.
     *
     * @var mixed
     */
    private $date;

    /**
     * Parses DQL expression.
     *
     * @param Parser $parser DQL parser
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->date = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * Produces SQL expression.
     *
     * @param SqlWalker $sqlWalker Sql helper class
     *
     * @return string MySQL native expression
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return 'DATE('.$this->date->dispatch($sqlWalker).')';
    }
}
