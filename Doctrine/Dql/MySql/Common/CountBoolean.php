<?php

namespace Mazurkiewicz\FuzzyBundle\Doctrine\Dql\MySql\Common;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Provides function that counts boolean values equal to true.
 * Produces native MySQL expression.
 */
class CountBoolean extends FunctionNode
{
    /**
     * Array of values passed to function.
     *
     * @var array
     */
    private $values = [];

    /**
     * Parses DQL expression.
     *
     * @param Parser $parser DQL parser
     */
    public function parse(Parser $parser)
    {
        $lexer = $parser->getLexer();
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->values[] = $parser->ArithmeticPrimary();

        while (Lexer::T_COMMA === $lexer->lookahead['type']) {
            $parser->match(Lexer::T_COMMA);
            $this->values[] = $parser->ArithmeticPrimary();
        }

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * Produces SQL expression.
     *
     * @param SqlWalker $sqlWalker Sql helper class
     *
     * @return string MySQL native expression
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        $values = [];
        foreach ($this->values as $value) {
            $values[] = 'CAST('.$value->dispatch($sqlWalker).' IS NOT NULL AS SIGNED INTEGER)';
        }

        return implode(' + ', $values);
    }
}
