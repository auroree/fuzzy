<?php

namespace Mazurkiewicz\FuzzyBundle\Doctrine\Dql\MySql\Fuzzy;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Provides fuzzy function: RANGE_UP(x, a, b).
 * Produces native MySQL expression.
 */
class RangeUp extends FunctionNode
{
    /**
     * Input of fuzzy function.
     *
     * @var string
     */
    private $x;

    /**
     * Fuzzy parameter: a.
     *
     * @var string
     */
    private $a;

    /**
     * Fuzzy parameter: b.
     *
     * @var string
     */
    private $b;

    /**
     * Parses DQL expression.
     *
     * @param Parser $parser DQL parser
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->x = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->a = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->b = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * Produces SQL expression.
     *
     * @param SqlWalker $sqlWalker Sql helper class
     *
     * @return string MySQL native expression
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        $x = $this->x->dispatch($sqlWalker);
        $a = $this->a->dispatch($sqlWalker);
        $b = $this->b->dispatch($sqlWalker);

        $output = "CASE
            WHEN $x <= $a THEN 0
            WHEN $x <= $b THEN ($x-$a)/($b-$a)
            ELSE 1 END";

        return $output;
    }
}
