<?php

namespace Mazurkiewicz\FuzzyBundle\Fuzzy\Functions;

/**
 * Base class for fuzzy function.
 */
abstract class AbstractFuzzyFunction
{
    /**
     * List of function parameters.
     *
     * @var array
     */
    private $parameters;

    /**
     * Constructor.
     *
     * @param array $parameters Function parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = array_values($parameters);
    }

    /**
     * Gets DQL condition expression.
     *
     * @param string $mode      Contant from class FuzzyModes
     * @param string $value     String representing value in sql statement
     * @param float  $threshold Threshold for fuzzy query
     */
    public function getDql($mode, $value, $threshold = false)
    {
        return $this->getDqlString($mode, $this->parameters, $value, $threshold);
    }

    /**
     * Produces function call string.
     *
     * @param string $identifier Function identifier
     * @param array  $parameters Function parameters
     *
     * @return string Function call string
     */
    protected function concatFunction($identifier, $parameters)
    {
        return $identifier.'('.implode(', ', $parameters).')';
    }

    /**
     * Gets DQL expression representing fuzzy function.
     *
     * @param \Mazurkiewicz\FuzzyBundle\Fuzzy\FuzzyModes $mode
     * @param array                                      $parameters Array of function parameters
     * @param string                                     $value      String representing value in sql statement
     * @param float                                      $threshold  Threshold for fuzzy query
     */
    abstract protected function getDqlString($mode, array $parameters, $value, $threshold);
}
