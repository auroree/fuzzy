<?php

namespace Mazurkiewicz\FuzzyBundle\Fuzzy\Functions;

use Mazurkiewicz\FuzzyBundle\Fuzzy\FuzzyModes;

/**
 * Represents Gaussian (NEAR_GAUSSIAN) fuzzy function.
 *
 * @author hmazurkiewicz
 */
class NearGaussianFunction extends AbstractFuzzyFunction
{
    /**
     * Constructor.
     *
     * @param array $params Function parameters
     */
    public function __construct(array $params)
    {
        parent::__construct($params);
    }

    /**
     * {@inheritdoc}
     */
    protected function getDqlString($mode, array $parameters, $value, $threshold)
    {
        // Put paremeters and value into one array:
        // 0 => value
        // 1 => a
        // 2 => b
        array_unshift($parameters, $value);
        $a = $parameters[1];

        // Build function call
        $expression = $this->concatFunction('NEAR_GAUSSIAN', $parameters);

        switch ($mode) {
            case FuzzyModes::NOT_IN_SET:
                $expression = "1 - $expression";
                break;

            case FuzzyModes::ABOVE_SET:
                $expression = "IF($value <= $a, 0, 1 - $expression)";
                break;

            case FuzzyModes::BELOW_SET:
                $expression = "IF($value >= $a, 0, 1 - $expression)";
                break;

            case FuzzyModes::IN_SET:
            default:
                break;
        }

        // Add threshold condition
        if ($threshold) {
            $expression .= '>=' . $threshold;
        }

        return $expression;
    }

}
