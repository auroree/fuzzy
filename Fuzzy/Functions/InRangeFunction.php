<?php

namespace Mazurkiewicz\FuzzyBundle\Fuzzy\Functions;

use Mazurkiewicz\FuzzyBundle\Fuzzy\FuzzyModes;

/**
 * Represents trapezium (IN_RANGE) fuzzy function.
 */
class InRangeFunction extends AbstractFuzzyFunction
{
    /**
     * Constructor.
     *
     * @param array $params Function parameters
     */
    public function __construct(array $params)
    {
        parent::__construct($params);
    }

    /**
     * {@inheritdoc}
     */
    protected function getDqlString($mode, array $parameters, $value, $threshold)
    {
        $expression = '';

        switch ($mode) {
            case FuzzyModes::NOT_IN_SET:
                array_unshift($parameters, $value);

                $expression = '1 - ' . $this->concatFunction('IN_RANGE', $parameters);
                break;

            case FuzzyModes::ABOVE_SET:
                $params = [
                    $value,
                    $parameters[2],
                    $parameters[3],
                ];

                $expression = $this->concatFunction('RANGE_UP', $params);
                break;

            case FuzzyModes::BELOW_SET:
                $params = [
                    $value,
                    $parameters[0],
                    $parameters[1],
                ];

                $expression = $this->concatFunction('RANGE_DOWN', $params);
                break;

            case FuzzyModes::IN_SET:
            default:
                array_unshift($parameters, $value);

                $expression = $this->concatFunction('IN_RANGE', $parameters);
                break;
        }

        // Add threshold condition
        if ($threshold) {
            $expression .= '>=' . $threshold;
        }

        return $expression;
    }

}
