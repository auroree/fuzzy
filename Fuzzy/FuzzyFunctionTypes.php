<?php

namespace Mazurkiewicz\FuzzyBundle\Fuzzy;

/**
 * Types of fuzzy function provider by library.
 */
class FuzzyFunctionTypes
{
    const IN_RANGE = 'range';
    const NEAR = 'near';
    const NEAR_GAUSSIAN = 'near_gaussian';
}
