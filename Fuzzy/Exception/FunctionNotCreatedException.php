<?php

namespace Mazurkiewicz\FuzzyBundle\Fuzzy\Exception;

/**
 * Exception thrown when non-existent function was requested from factory.
 */
class FunctionNotCreatedException extends \Exception
{
    /**
     * Constructor.
     *
     * @param string $functionType Constant from class FuzzyFunctionTypes
     */
    public function __construct($functionType)
    {
        $message = 'Cannot create non-existent function "'.$functionType.'".';
        parent::__construct($message);
    }
}
