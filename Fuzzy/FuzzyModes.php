<?php

namespace Mazurkiewicz\FuzzyBundle\Fuzzy;

/**
 * Available modes for creating DQL expression.
 */
class FuzzyModes
{
    /**
     * "none" mode cannot be requested from function class.
     */
    const NONE = 'none';
    const IN_SET = 'in_set';
    const NOT_IN_SET = 'not_in_set';
    const ABOVE_SET = 'above_set';
    const BELOW_SET = 'below_set';
}
