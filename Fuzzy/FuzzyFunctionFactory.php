<?php

namespace Mazurkiewicz\FuzzyBundle\Fuzzy;

/**
 * Creates fuzzy function class for given type.
 */
class FuzzyFunctionFactory
{
    /**
     * Creates fuzzy function for given type and parameters.
     *
     * @param string $type       Constant from class FuzzyFunctionTypes
     * @param array  $parameters Function parameters
     *
     * @throws Exception\FunctionNotCreatedException If invalid function type was requested
     */
    public static function create($type, array $parameters)
    {
        switch ($type) {
            case FuzzyFunctionTypes::NEAR:
                return new Functions\NearFunction($parameters);

            case FuzzyFunctionTypes::IN_RANGE:
                return new Functions\InRangeFunction($parameters);

            case FuzzyFunctionTypes::NEAR_GAUSSIAN:
                return new Functions\NearGaussianFunction($parameters);
        }

        throw new Exception\FunctionNotCreatedException($type);
    }
}
