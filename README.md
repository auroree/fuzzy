# FuzzyBundle

## Installation

1. Add bundle to dependencies
    Add git repository to composer configuration:
    
    ```
    "repositories": [
      {
        "type": "vcs",
        "url": "https://gitlab.com/auroree/fuzzy"
      }
    ],
    ```
    
    Add bundle dependency to composer by running console command
    
    `composer require mazurkiewicz/fuzzy-bundle:dev-master`
    
    or add it to composer configuration:
    
    ```
    "require": {
      "mazurkiewicz/fuzzy-bundle": "dev-master"
    },
    ```
    
    and run `composer update`.
    
2. Enable bundle
    Add bundle to AppKernel class
    
    ```
    public function registerBundles()
    {
      $bundles = [
        // ...
        new Mazurkiewicz\FuzzyBundle\MazurkiewiczFuzzyBundle()
      ];
    }
    ```
    
3. Add DQL functions to configuration in app/config.yml file
    Import parameters.yml file
    
    ```
    imports:
      # ...
      - { resource: '@MazurkiewiczFuzzyBundle/Resources/config/parameters.yml' }
    ```
    
    Add DQL functions to application configuration.
    
    ```
    doctrine:
      orm:
        dql: "%fuzzy_mysql%"
    ```
    
## Examples
```
use Mazurkiewicz\FuzzyBundle\Fuzzy\FuzzyFunctionFactory;
use Mazurkiewicz\FuzzyBundle\Fuzzy\FuzzyFunctionTypes;
use Mazurkiewicz\FuzzyBundle\Fuzzy\FuzzyModes;
use AppBundle\Entity\ExampleEntity;

$repository = $em->getRepository(ExampleEntity::class);

// Example 1 - Select membership function result

$function = FuzzyFunctionFactory::create(FuzzyFunctionTypes::NEAR, [5, 15, 20]);
$fuzzyDql = $function->getDql(FuzzyModes::IN_SET, 'e.value');

$query = $repository->createQueryBuilder('e')
  ->select("e.value, $fuzzyDql AS membership_degree")
  ->getQuery();

// SELECT e.value, NEAR(e.value, 5, 15, 20) AS membership_degree
// FROM AppBundle\Entity\ExampleEntity e
```

```
// Example 2 - Get conditional expression by passing 3rd parameter to getDql - threshold

$function = FuzzyFunctionFactory::create(FuzzyFunctionTypes::NEAR, [5, 15, 20]);
$fuzzyDqlCondition = $function->getDql(FuzzyModes::IN_SET, 'e.value', 0.3);

$query = $repository->createQueryBuilder('e')
  ->select('e.value')
  ->where($fuzzyDqlCondition)
  ->getQuery();

// SELECT e.value
// FROM AppBundle\Entity\ExampleEntity e
// WHERE NEAR(e.value, 5, 15, 20)>=0.3
```

```
// Example 3 - Use function as both: selection result and condition

$function = FuzzyFunctionFactory::create(FuzzyFunctionTypes::NEAR, [5, 15, 20]);
$fuzzyDqlCondition = $function->getDql(FuzzyModes::ABOVE_SET, 'e.value', 0.3);
$fuzzyDql = $function->getDql(FuzzyModes::ABOVE_SET, 'e.value');

$query = $repository->createQueryBuilder('e')
  ->select('e.value, '.$fuzzyDql.' AS membership_degree')
  ->where($fuzzyDqlCondition)
  ->getQuery();

// SELECT e.value, RANGE_UP(e.value, 15, 20) AS membership_degree
// FROM AppBundle\Entity\ExampleEntity e
// WHERE RANGE_UP(e.value, 15, 20)>=0.3
```

```
// Example 4 - Example of other function: NEAR_GAUSSIAN

$fuzzyDql = FuzzyFunctionFactory::create(FuzzyFunctionTypes::NEAR_GAUSSIAN, [20, 10])
  ->getDql(FuzzyModes::BELOW_SET, 'e.value');

$query = $repository->createQueryBuilder('e')
  ->select('e.value, '.$fuzzyDql.' AS membership_degree')
  ->getQuery();

// SELECT e.value, IF(e.value >= 20, 0, 1 - NEAR_GAUSSIAN(e.value, 20, 10)) AS membership_degree
// FROM AppBundle\Entity\ExampleEntity e
```
