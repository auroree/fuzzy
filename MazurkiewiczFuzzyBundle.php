<?php

namespace Mazurkiewicz\FuzzyBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Fuzzy bundle declaration.
 */
class MazurkiewiczFuzzyBundle extends Bundle
{
}
