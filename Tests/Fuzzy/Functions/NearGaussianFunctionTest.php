<?php

namespace Mazurkiewicz\FuzzyBundle\Tests\Fuzzy\Functions;

use Mazurkiewicz\FuzzyBundle\Fuzzy\Functions\NearGaussianFunction;
use Mazurkiewicz\FuzzyBundle\Fuzzy\FuzzyModes;
use PHPUnit_Framework_TestCase;

/**
 * Tests DQL expressions creation for NEAR_GAUSSIAN function.
 */
class NearGaussianFunctionTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider dqlDataProvider
     *
     * @param array  $params
     * @param string $mode
     * @param string $value
     * @param string $expectedDql
     */
    public function testDql($params, $mode, $value, $expectedDql)
    {
        $function = new NearGaussianFunction($params);
        $dql = $function->getDql($mode, $value);

        $this->assertEquals($expectedDql, $dql);
    }

    /**
     * Provides test data.
     *
     * @return array Test data sets
     */
    public function dqlDataProvider()
    {
        return [
            [
                [10, 5],
                FuzzyModes::IN_SET,
                'a.column',
                'NEAR_GAUSSIAN(a.column, 10, 5)',
            ],
            [
                [10, 5],
                FuzzyModes::NONE,
                'a.column',
                'NEAR_GAUSSIAN(a.column, 10, 5)',
            ],
            [
                [10, 5],
                FuzzyModes::NOT_IN_SET,
                'a.column',
                '1 - NEAR_GAUSSIAN(a.column, 10, 5)',
            ],
            [
                [10, 5],
                FuzzyModes::BELOW_SET,
                'a.column',
                'IF(a.column >= 10, 0, 1 - NEAR_GAUSSIAN(a.column, 10, 5))',
            ],
            [
                [10, 5],
                FuzzyModes::ABOVE_SET,
                'a.column',
                'IF(a.column <= 10, 0, 1 - NEAR_GAUSSIAN(a.column, 10, 5))',
            ],
        ];
    }

    /**
     * @dataProvider dqlConditionDataProvider
     *
     * @param array  $params
     * @param string $mode
     * @param string $value
     * @param string $expectedDql
     */
    public function testDqlCondition($params, $mode, $value, $threshold, $expectedDql)
    {
        $function = new NearGaussianFunction($params);
        $dql = $function->getDql($mode, $value, $threshold);

        $this->assertEquals($expectedDql, $dql);
    }

    /**
     * Provides test data.
     *
     * @return array Test data sets
     */
    public function dqlConditionDataProvider()
    {
        return [
            [
                [10, 5],
                FuzzyModes::IN_SET,
                'a.column',
                0.3,
                'NEAR_GAUSSIAN(a.column, 10, 5)>=0.3',
            ],
            [
                [10, 5],
                FuzzyModes::NONE,
                'a.column',
                0.3,
                'NEAR_GAUSSIAN(a.column, 10, 5)>=0.3',
            ],
            [
                [10, 5],
                FuzzyModes::NOT_IN_SET,
                'a.column',
                0.3,
                '1 - NEAR_GAUSSIAN(a.column, 10, 5)>=0.3',
            ],
            [
                [10, 5],
                FuzzyModes::BELOW_SET,
                'a.column',
                0.3,
                'IF(a.column >= 10, 0, 1 - NEAR_GAUSSIAN(a.column, 10, 5))>=0.3',
            ],
            [
                [10, 5],
                FuzzyModes::ABOVE_SET,
                'a.column',
                0.3,
                'IF(a.column <= 10, 0, 1 - NEAR_GAUSSIAN(a.column, 10, 5))>=0.3',
            ],
        ];
    }
}
