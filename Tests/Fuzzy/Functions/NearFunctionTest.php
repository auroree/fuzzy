<?php

namespace Mazurkiewicz\FuzzyBundle\Tests\Fuzzy\Functions;

use Mazurkiewicz\FuzzyBundle\Fuzzy\Functions\NearFunction;
use Mazurkiewicz\FuzzyBundle\Fuzzy\FuzzyModes;
use PHPUnit_Framework_TestCase;

/**
 * Tests DQL expressions creation for NEAR function.
 */
class NearFunctionTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider dqlDataProvider
     *
     * @param array  $params
     * @param string $mode
     * @param string $value
     * @param string $expectedDql
     */
    public function testDql($params, $mode, $value, $expectedDql)
    {
        $function = new NearFunction($params);
        $dql = $function->getDql($mode, $value);

        $this->assertEquals($expectedDql, $dql);
    }

    /**
     * Provides test data.
     *
     * @return array Test data sets
     */
    public function dqlDataProvider()
    {
        return [
            [
                [1, 10, 21],
                FuzzyModes::IN_SET,
                'a.column',
                'NEAR(a.column, 1, 10, 21)',
            ],
            [
                [1, 10, 21],
                FuzzyModes::NONE,
                'a.column',
                'NEAR(a.column, 1, 10, 21)',
            ],
            [
                [1, 10, 21],
                FuzzyModes::NOT_IN_SET,
                'a.column',
                '1 - NEAR(a.column, 1, 10, 21)',
            ],
            [
                [1, 10, 21, 30],
                FuzzyModes::BELOW_SET,
                'a.column',
                'RANGE_DOWN(a.column, 1, 10)',
            ],
            [
                [1, 10, 21, 30],
                FuzzyModes::ABOVE_SET,
                'a.column',
                'RANGE_UP(a.column, 10, 21)',
            ],
        ];
    }

    /**
     * @dataProvider dqlConditionDataProvider
     *
     * @param array  $params
     * @param string $mode
     * @param string $value
     * @param string $expectedDql
     */
    public function testDqlCondition($params, $mode, $value, $threshold, $expectedDql)
    {
        $function = new NearFunction($params);
        $dql = $function->getDql($mode, $value, $threshold);

        $this->assertEquals($expectedDql, $dql);
    }

    /**
     * Provides test data.
     *
     * @return array Test data sets
     */
    public function dqlConditionDataProvider()
    {
        return [
            [
                [1, 10, 21],
                FuzzyModes::IN_SET,
                'a.column',
                0.3,
                'NEAR(a.column, 1, 10, 21)>=0.3',
            ],
            [
                [1, 10, 21],
                FuzzyModes::NONE,
                'a.column',
                0.3,
                'NEAR(a.column, 1, 10, 21)>=0.3',
            ],
            [
                [1, 10, 21],
                FuzzyModes::NOT_IN_SET,
                'a.column',
                0.3,
                '1 - NEAR(a.column, 1, 10, 21)>=0.3',
            ],
            [
                [1, 10, 21],
                FuzzyModes::BELOW_SET,
                'a.column',
                0.3,
                'RANGE_DOWN(a.column, 1, 10)>=0.3',
            ],
            [
                [1, 10, 21],
                FuzzyModes::ABOVE_SET,
                'a.column',
                0.3,
                'RANGE_UP(a.column, 10, 21)>=0.3',
            ],
        ];
    }
}
