<?php

namespace Mazurkiewicz\FuzzyBundle\Tests\Doctrine\Dql\MySql\Fuzzy;

/**
 * Tests RANGE_DOWN function.
 */
class RangeDownTest extends FuzzyTestBase
{
    /**
     * @dataProvider valuesProvider
     *
     * @param float $value    Tested function input value
     * @param float $expected Expected result value
     */
    public function testRangeUp($value, $expected)
    {
        $params = [
            4,
            7,
        ];

        $column = 't.value';
        $inRange = sprintf('RANGE_DOWN(%s, %s)', $column, implode(',', $params));
        $rows = $this->getRepository()->createQueryBuilder('t')
            ->select("$column AS value, $inRange AS result")
            ->where('t.value = '.$value)
            ->getQuery()
            ->getArrayResult();

        $this->assertEquals($expected, $rows[0]['result'], 0.000001);
    }

    /**
     * Provides test data.
     *
     * @return array Test data sets
     */
    public function valuesProvider()
    {
        return [
            [-2,  1],
            [0,   1],
            [1,   1],
            [4,   1],
            [5.5, 0.5],
            [7,   0],
            [10,  0],
        ];
    }
}
