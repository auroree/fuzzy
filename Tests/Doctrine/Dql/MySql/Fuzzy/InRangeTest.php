<?php

namespace Mazurkiewicz\FuzzyBundle\Tests\Doctrine\Dql\MySql\Fuzzy;

/**
 * Tests IN_RANGE function.
 */
class InRangeTest extends FuzzyTestBase
{
    /**
     * @dataProvider valuesProvider
     *
     * @param float $value    Tested function input value
     * @param float $expected Expected result value
     */
    public function testInRange($value, $expected)
    {
        $params = [
            2,
            4,
            5,
            6,
        ];

        $column = 't.value';
        $inRange = sprintf('IN_RANGE(%s, %s)', $column, implode(',', $params));
        $rows = $this->getRepository()->createQueryBuilder('t')
            ->select("$column AS value, $inRange AS result")
            ->where('t.value = '.$value)
            ->getQuery()
            ->getArrayResult();

        $this->assertEquals($expected, $rows[0]['result'], 0.000001);
    }

    /**
     * Provides test data.
     *
     * @return array Test data sets
     */
    public function valuesProvider()
    {
        return [
            [-2,  0],
            [0,   0],
            [1,   0],
            [2,   0],
            [3,   0.5],
            [4,   1],
            [4.5, 1],
            [5,   1],
            [5.5, 0.5],
            [6,   0],
            [10,  0],
        ];
    }
}
