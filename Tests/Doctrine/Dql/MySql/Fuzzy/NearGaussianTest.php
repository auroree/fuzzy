<?php

namespace Mazurkiewicz\FuzzyBundle\Tests\Doctrine\Dql\MySql\Fuzzy;

/**
 * Tests NEAR_GAUSSIAN function.
 */
class NearGaussianTest extends FuzzyTestBase
{
    /**
     * @dataProvider valuesProvider
     *
     * @param float $value    Tested function input value
     * @param float $expected Expected result value
     */
    public function testNear($value, $expected)
    {
        $params = [
            5,
            2,
        ];

        $column = 't.value';
        $inRange = sprintf('NEAR_GAUSSIAN(%s, %s)', $column, implode(',', $params));
        $rows = $this->getRepository()->createQueryBuilder('t')
            ->select("$column AS value, $inRange AS result")
            ->where('t.value = '.$value)
            ->getQuery()
            ->getArrayResult();

        $this->assertEquals($expected, $rows[0]['result'], '', 0.000001);
    }

    /**
     * Provides test data.
     *
     * @return array Test data sets
     */
    public function valuesProvider()
    {
        return [
            [-2,  0.000005],
            [0,   0.001930],
            [0.5, 0.006330],
            [1,   0.018316],
            [2.1, 0.122151],
            [5,   1],
            [5.1, 0.997503],
            [6.4, 0.612626],
            [20,  0.000000],
        ];
    }
}
