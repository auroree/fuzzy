<?php

namespace Mazurkiewicz\FuzzyBundle\Tests\Doctrine\Dql\MySql\Fuzzy;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Mazurkiewicz\FuzzyBundle\DataFixtures\ORM\TestDataFixture;
use Mazurkiewicz\FuzzyBundle\Tests\Table\TestData;
use PHPUnit_Framework_TestCase;

/**
 * Base class for fuzzy function tests.
 * Clears test_data table and inserts test values.
 */
class FuzzyTestBase extends PHPUnit_Framework_TestCase
{
    /**
     * Doctrine entity manager.
     *
     * @var EntityManager
     */
    private $em;

    /**
     * TestData repository.
     *
     * @var EntityRepository
     */
    private $repository;

    protected function getRepository()
    {
        return $this->repository;
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->em = getEntityManager();
        $this->repository = $this->em->getRepository(TestData::class);

        $metaData = $this->em->getClassMetadata(TestData::class);
        $connection = $this->em->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->executeUpdate($dbPlatform->getTruncateTableSql($metaData->getTableName()));

        $fixture = new TestDataFixture();
        $fixture->load($this->em);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        $this->em->close();
        $this->em = null;

        parent::tearDown();
    }

}
