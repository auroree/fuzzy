<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\StaticPHPDriver;
use Doctrine\ORM\Tools\Setup;
use Mazurkiewicz\FuzzyBundle\Doctrine\Dql\MySql\Fuzzy;
use Symfony\Component\Yaml\Yaml;

require_once 'vendor/autoload.php';
define('ROOT_DIR', __DIR__);

/**
 * Instantiates EntityManager.
 * @param array $dbParams
 * @return EntityManager
 */
function getEntityManager($dbParams = [])
{
    $paths = [ROOT_DIR . '/Tests/Table'];
    $isDevMode = true;

    if (!$dbParams) {
        $bundleConfig = Yaml::parse(file_get_contents(ROOT_DIR . '/config.yml'));
        $dbParams = $bundleConfig['db'];
    }

//    $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
    $config = Setup::createConfiguration($isDevMode);
    $driver = new StaticPHPDriver($paths);
    $config->setMetadataDriverImpl($driver);

    // Add DQL functions
    $config->addCustomNumericFunction('near_gaussian', Fuzzy\NearGaussian::class);
    $config->addCustomNumericFunction('near', Fuzzy\Near::class);
    $config->addCustomNumericFunction('in_range', Fuzzy\InRange::class);
    $config->addCustomNumericFunction('range_up', Fuzzy\RangeUp::class);
    $config->addCustomNumericFunction('range_down', Fuzzy\RangeDown::class);

    $entityManager = EntityManager::create($dbParams, $config);

    return $entityManager;
}
