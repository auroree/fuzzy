<?php

namespace Mazurkiewicz\FuzzyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mazurkiewicz\FuzzyBundle\Tests\Table\TestData;

/**
 * Creates test data fixture.
 */
class TestDataFixture implements FixtureInterface
{
    /**
     * Loads test data to database.
     *
     * @param ObjectManager $manager Object manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = -2.5; $i < 20.1; $i += 0.1) {
            $manager->persist(new TestData(round($i, 1)));
        }

        $manager->flush();
    }
}
